# uzb-tesseract-langdata
Tesseract source training data for Uzbek

З.М. Маъруфов таҳрири остида 1981 йилда чоп этилган «Ўзбек тилининг
изоҳли луғати»ни таҳлилига мўлжалланган машқ маълумотлари.

Trained data can be found at https://github.com/bmansurov/uzb-tesseract-tessdata.

Requirements
============
* Fedora 25
* `tesseract` version 3.04 and `tesseract-langpack-uzb_cyrl` packages.
* Actually for better results, the langpack package should be recompiled
without support for Latin as the text in the scanned text is in Cyrillic
only. This can be done by clonging
[the original data] (https://github.com/tesseract-ocr/langdata) and 
removing the file `uzb_cyrl/uzb_cyrl.config` then training the langdata.
* `git`
* Depending on how far you want to go, you may also need `Ghostscript`,
[jTessBoxEditor] (https://sourceforge.net/projects/vietocr/files/jTessBoxEditor/)
and `gimp`.

Howto
=====
1. Create dirs we'll be using: `mkdir lib && mkdir out && cd out`. `lib`
will be used for github repos we'll need to clone to get the job done.
`out` will be used as our working directory.

2. Generate `tif` and `box` files from the typed text.
	`$ git clone https://github.com/bmansurov/adabiy-font.git ../lib/adabiy-font`
	`$ text2image --text=../uzb_cyrl/uzb_cyrl.input_text --outputbase=uzb_cyrl.Adabiy.exp0 --font='Adabiy' --fonts_dir=../lib/adabiy-font`
	`$ text2image --text=../uzb_cyrl/uzb_cyrl.input_text --outputbase=uzb_cyrl.AdabiyBold.exp0 --font='Adabiy Bold' --fonts_dir=../lib/adabiy-font`
	`$ text2image --text=../uzb_cyrl/uzb_cyrl.input_text --outputbase=uzb_cyrl.AdabiyItalic.exp0 --font='Adabiy Italic' --fonts_dir=../lib/adabiy-font --char_spacing=0.4`
Notice the `char-spacing` option above. We add it so that italic fonts are
output some distance apart from each other and are easier to recognize.

3. Train the box files:
	`$ tesseract uzb_cyrl.Adabiy.exp0.tif uzb_cyrl.Adabiy.exp0 box.train.stderr`
	`$ tesseract uzb_cyrl.AdabiyBold.exp0.tif uzb_cyrl.AdabiyBold.exp0 box.train.stderr`
	`$ tesseract uzb_cyrl.AdabiyItalic.exp0.tif uzb_cyrl.AdabiyItalic.exp0 box.train.stderr`

4. Extract unicharset:
	`$ unicharset_extractor uzb_cyrl*.box`
	`$ mv unicharset unicharset_input`
	`$ # TODO: add heights for Adabiy font to ../uzb_cyrl/langdata/Cyrillic.xheights and Latin.xheights in the same directory`
	`$ set_unicharset_properties -U unicharset_input -O unicharset --script_dir=../uzb_cyrl/langdata`
	`$ rm unicharset_input`
The contents of the langdata folder and `font_properties` (with some
modifications) in the next step is taken from
[source training data for Tesseract] (`https://github.com/tesseract-ocr/langdata`)

5. Shape clustering and training
	`$ shapeclustering -F ../uzb_cyrl/font_properties -U unicharset uzb_cyrl*.tr`
	`$ mftraining -F ../uzb_cyrl/font_properties -U unicharset -O uzb_cyrl.unicharset uzb_cyrl*.tr`
	`$ cntraining uzb_cyrl*.tr`

6. Clone neeeded words files and generate `dawg` files
	`$ git clone https://github.com/bmansurov/uzb-frequent-words.git ../lib/uzb-frequent-words`
	`$ git clone https://github.com/bmansurov/uzb-dict-words.git ../lib/uzb-dict-words`
	`$ wordlist2dawg ../lib/uzb-frequent-words/frequent_words_list.txt uzb_cyrl.freq-dawg unicharset`
	`$ wordlist2dawg ../lib/uzb-dict-words/words_list.txt uzb_cyrl.word-dawg unicharset`
	`$ wordlist2dawg ../uzb_cyrl/punc.txt uzb_cyrl.punc-dawg unicharset`
	`$ wordlist2dawg ../uzb_cyrl/number.txt uzb_cyrl.number-dawg unicharset`

7. Copy over some files and move others:
	`$ cp ../uzb_cyrl/uzb_cyrl.config ./`
	`$ cp ../uzb_cyrl/uzb_cyrl.unicharambigs ./`
	`$ mv unicharset uzb_cyrl.unicharset`
	`$ mv inttemp uzb_cyrl.inttemp`
	`$ mv normproto uzb_cyrl.normproto`
	`$ mv pffmtable uzb_cyrl.pffmtable`
	`$ mv shapetable uzb_cyrl.shapetable`

8. Combine the generated files and move the trained data to the shared folder:
	`$ combine_tessdata uzb_cyrl.`
	`$ sudo cp uzb_cyrl.traineddata /usr/share/tesseract/tessdata/uzb_cyrl_2.traineddata`

9. Test one of the tif images that were used in training the data:
	`$ tesseract uzb_cyrl.Adabiy.exp0.tif test -l uzb_cyrl_2`
Check the test.txt and see how well the words in `uzb_cyrl.Adabiy.exp0.tif`
were recognized.

10. This traineddata can now be used to train other models that fit specific
books. You can use the model as is too, but if you want better results see
next steps.

11. Assuming the book you want to recognize is called `../book.pdf` extract a
page from it. Since this model is separate from the above one, you'll need to
clean up the contents of the current one, i.e. `out` directory first.
	`$ rm -r ./*`
	`$ gs -dNOPAUSE -q -r500 -sDEVICE=tiffg4 -dBATCH -sOutputFile=uzb_cyrl.Adabiy.exp0.tif -dFirstPage=10 -dLastPage=10 ../book.pdf`

12. Copy over the above `tif` file and rename as so:
	`$ cp uzb_cyrl.Adabiy.exp0.tif uzb_cyrl.AdabiyBold.exp0.tif`
	`$ cp uzb_cyrl.Adabiy.exp0.tif uzb_cyrl.AdabiyItalic.exp0.tif`
Edit all three `tif` files with `gimp` and remove all italic and bold texts
from the first one, all normal and italic from the second one, and normal
and bold from the third one. If bold texts are not enough, for example, you'll
need to extract other pages from the book where there is text in bold and use
these pages for training. You'll just name the newly extracted pages as so:
`uzb_cyrl.AdabiyBold.exp1.tif`, `uzb_cyrl.AdabiyBold.exp2.tif`, etc.

13. Generate box files:
	`$ tesseract uzb_cyrl.Adabiy.exp0.tif uzb_cyrl.Adabiy.exp0 -l uzb_cyrl_2 batch.nochop makebox`
	`$ tesseract uzb_cyrl.AdabiyBold.exp0.tif uzb_cyrl.AdabiyBold.exp0 -l uzb_cyrl_2 batch.nochop makebox`
	`$ tesseract uzb_cyrl.AdabiyItalic.exp0.tif uzb_cyrl.AdabiyItalic.exp0 -l uzb_cyrl_2 batch.nochop makebox`

14. Do steps 3 through 9, only copying over the traineddata file as
`uzb_cyrl_3.traineddata` in step 8.

15. That's it, you now how a custom trained model for your book.

Ушбу машқ маълумотлари Apache License v2.0 билан лицензияланган.
